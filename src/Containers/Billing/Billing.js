// @flow
import React, { Component } from 'react';

import Aux from '../../Components/Auxx/Auxx';
import * as _billingApi from '../../Services/Billing.Api';
import BillingUtils from '../../Utils/billing/billing.utils';

import BillTotal from '../../Components/BillTotal/BillTotal';
import BillList from '../../Components/BillList/BillList';

class Billing extends Component {

    state: Object = {
        billTotal: {
            total: 0,
            paid: 0,
            overdue: 0,
            outstanding: 0
        },
        list: []
    }

    componentDidMount() {
        const billObj = _billingApi.getBilling();
        const billJson = BillingUtils.transformData(billObj)
        this.updateState(billJson);
    }

    updateState(arr: Array<{}>): void {
        this.setState(
            {
                billTotal: 
                {
                    total: BillingUtils.getTotalBill(arr),
                    paid: BillingUtils.getTotalBillOfType(arr, 'paid'),
                    overdue: BillingUtils.getTotalBillOfType(arr, 'overdue'),
                    outstanding: BillingUtils.getTotalBillOfType(arr, 'outstanding')
                },
                list: BillingUtils.groupByDescendingDate(arr)
        });
    }

    render(): JSX {
        return (
            <Aux>
                <BillTotal
                    total = {this.state.billTotal.total}
                    paid = {this.state.billTotal.paid}
                    overdue = {this.state.billTotal.overdue}
                    outstanding = {this.state.billTotal.outstanding}
                />
                <BillList
                    list = {this.state.list}
                />
            </Aux>
        )
    }
}

export default Billing;