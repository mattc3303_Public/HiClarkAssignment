// @flow
import React from 'react';

import Aux from '../Components/Auxx/Auxx';
import Billing from './Billing/Billing';

const App = () =>
    <Aux>
        <Billing />
    </Aux>

export default App;
