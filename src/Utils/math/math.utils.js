// @flow
const MathUtils = {
    sum: (arr: Array<any>): number => {
        return arr.reduce( (acc, cur) => {
            return acc + cur;
        }, 0)
    },

    convertCentsToDollars: (cents: numbers): string => 
        (cents / 100).toFixed(2).toLocaleString()
}

export default MathUtils;