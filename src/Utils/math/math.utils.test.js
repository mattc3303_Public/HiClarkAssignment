import MathUtils from '../math/math.utils';

test('Should return a number', () => {
    expect(MathUtils.sum([1,2,3])).toBe(6)
})

test('Should return as a string in dollars format', () => {
    expect(MathUtils.convertCentsToDollars(100)).toBe('1.00')
})