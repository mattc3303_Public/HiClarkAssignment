// @flow
const DateUtils = {
    convertEpochDate: (dateString: string): number => { 
        dateString += '-00-00-00';
        const d = dateString.split('-');
        return (new Date(d[0], d[1] - 1, d[2], d[3], d[4], d[5])).valueOf();
    }
}

export default DateUtils;