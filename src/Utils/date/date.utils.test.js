import DateUtils from '../date/date.utils';

test('Should return an equivalent epoch time', () => {
    const mockDate = '2018-08-10-00-00-00'
    expect(DateUtils.convertEpochDate(mockDate)).toBe(1533873600000)
})