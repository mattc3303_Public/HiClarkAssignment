// @flow
import _mathUtils from '../math/math.utils';
import _dateUtils from '../date/date.utils';

class BillingUtils {

    static getTotalBillOfType = (arr: Array<{}>, statusType: string): number => {
        const bill = arr.reduce( (acc, cur) => {
            let amountInCents = 0;
            if ( cur.status === statusType ) {
                amountInCents = cur.amountInCents;
            }
            acc += amountInCents;
            return acc;
        }, 0);
        const total = bill / 100;
        return total.toFixed(2).toLocaleString();
    }

    static getTotalBill(arr: Array<{}>): number {
        const bill = arr.map( eaItem => eaItem.amountInCents);
        const total = _mathUtils.sum(bill) / 100;
        return total.toFixed(2).toLocaleString();
    }

    static transformData(billObj: Array<{}>): Array<[]> {
        const currentEpochTime = Date.now();
        const adjustObjArr = Object.keys(billObj).map( key => {
            const eaItem = billObj[key];
            eaItem.epochTime = _dateUtils.convertEpochDate(eaItem.dueDate)
            eaItem.amountInDollars = _mathUtils.convertCentsToDollars(eaItem.amountInCents);
            if (eaItem.status === 'pending') {
                currentEpochTime >= eaItem.epochTime ? eaItem.status = 'overdue' : eaItem.status = 'outstanding';
            } 
            return eaItem;
        });
        return adjustObjArr;
    }

    static groupByKey(json: Array<{}>, key: string): Array<{}> {
        const data = json.reduce( (acc, cur) => {
            acc[cur[key]] = acc[cur[key]] || [];
            acc[cur[key]].push(cur);
            return acc;
        }, {});
        const dataArr = [];
        for (const eaItem in data) {
            if ( eaItem ) {
                dataArr.push(data[eaItem]);
            }
        }
        return dataArr;
    }

    static groupByDescendingDate(json: Array<[]>): Array<{}> {
        const groupedJson = this.groupByKey(json, 'status');
        const arr = [];
        groupedJson.forEach( eaItem => {
            const sorted = eaItem.sort( (a, b) => b.epochTime - a.epochTime);
            arr.push(sorted);
        })
        const flatten = [].concat(arr[2], arr[0], arr[1]);
        return flatten;
    }

}

export default BillingUtils;