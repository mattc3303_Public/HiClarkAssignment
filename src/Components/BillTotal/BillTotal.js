import React from 'react';
import './BillTotal.css';

const BillTotal = (props) => 
    <div className='billTotalContainer'>
        <span>total: ${props.total}</span>
        <span>paid: ${props.paid}</span>
        <span>overdue: ${props.overdue}</span>
        <span>outstanding: ${props.outstanding}</span>
    </div>

export default BillTotal;

