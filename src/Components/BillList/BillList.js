// @flow
import React from 'react';

const BillList = (props): JSX => {
    return (
        <ul>
            {
                props.list.map( i => 
                    <li key={i.id}>{`${i.id}: $${i.amountInDollars}, ${i.status}, ${i.dueDate}`}</li>)
            }
        </ul>
    )
}

export default BillList;