# Clark Take-Home Code Challenge

To get things running:

1. install [yarn](https://yarnpkg.com/lang/en/docs/install)
2. run `yarn install` to install dependencies
3. run `yarn start` to start the development server
4. run `yarn test` to run tests

